package com.boots.entity;

        import org.junit.Test;
        import org.junit.runner.RunWith;

        import org.springframework.boot.test.context.SpringBootTest;
        import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

        import javax.persistence.EntityManager;
        import javax.persistence.PersistenceContext;

        import java.util.List;

        import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserTest {


    @PersistenceContext
    EntityManager em;

    @Test
    public void getId() {
        User user = new User();
        assertEquals("Returned param id is wrong. Test U1.",null, user.getId());
        user.setId(89L);
        assertEquals("Failed to set id or id is wrong. Test U2.", java.util.Optional.of(89L), java.util.Optional.of(user.getId()));

        Long u_id = Integer.toUnsignedLong(35);
        List<User> main_user = em.createQuery("Select u from User u where u.id = :u_id")
                .setParameter("u_id", u_id).getResultList();
        assertEquals(java.util.Optional.of(Integer.toUnsignedLong(35)), java.util.Optional.of(main_user.get(0).getId()));
    }

    @Test
    public void setId() {
        User user = new User();
        user.setId(76L);
        assertEquals("Failed to set id or id is wrong. Test U3.", java.util.Optional.of(76L), java.util.Optional.of(user.getId()));
    }

    @Test
    public void setUsername() {
        User user = new User();
        assertEquals("Returned param id is wrong. Test U4.",null, user.getUsername());
        user.setUsername("testuser");
        assertEquals("Failed to set username or username is wrong. Test U5.", "testuser", user.getUsername());


    }



    @Test
    public void getUsername() {
        User user = new User();
        user.setUsername("testuser");
        assertEquals("Usernames are not equal. Test U6.", "testuser", user.getUsername());
        User user2 = new User();
        user2.setUsername("testuser");
        assertEquals("Usernames are not equal. Test U7.", "testuser", user2.getUsername());
        assertEquals("Usernames are not equal. Test U8.", user.getUsername(), user2.getUsername());

        Long u_id = Integer.toUnsignedLong(35);
        List<User> main_user = em.createQuery("Select u from User u where u.id = :u_id")
                .setParameter("u_id", u_id).getResultList();
        assertEquals("mastafev", main_user.get(0).getUsername());
    }
}