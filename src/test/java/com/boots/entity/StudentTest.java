package com.boots.entity;

import com.boots.repository.StudentRepository;
import com.boots.service.StudentService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class StudentTest {


    @Test
    public void getId() {
        Student student = new Student();
        assertEquals("Returned param id is wrong. Test S1.",null, student.getId());
        student.setId(89L);
        assertEquals("Failed to set id or id is wrong. Test S2.", java.util.Optional.of(89L), java.util.Optional.of(student.getId()));

    }

    @Test
    public void setId() {
        Student student = new Student("test","testtest");
        student.setId(89L);
        assertEquals("Failed to set id or id is wrong. Test S4.", java.util.Optional.of(89L), java.util.Optional.of(student.getId()));
    }

    @Test
    public void getName() {
        Student student = new Student();
        assertEquals("Names are not equal. Test S4.", null, student.getName());
        student.setName("testname");
        assertEquals("Names are not equal. Test S5.", "testname", student.getName());
    }

    @Test
    public void setName() {
        Student student = new Student();
        student.setName("testname");
        assertEquals("Names are not equal. Test S6.", "testname", student.getName());
        Student st2 = new Student();
        st2.setName("testname");
        assertEquals("Names are not equal. Test S7.", "testname", st2.getName());
        assertEquals("Names are not equal. Test S8.", st2.getName(), student.getName());
    }

    @Test
    public void getSurname() {
        Student student = new Student();
        assertEquals("Surnames are not equal. Test S9.", null, student.getSurname());
        student.setSurname("testsurname");
        assertEquals("Surnames are not equal. Test S10.", "testsurname", student.getSurname());
    }

    @Test
    public void setSurname() {
        Student student = new Student();
        assertEquals("Surnames are not equal. Test S11.", null, student.getSurname());
        student.setSurname("testsurname");
        assertEquals("Surnames are not equal. Test S12.", "testsurname", student.getSurname());
        Student st2 = new Student();
        st2.setSurname("testsurname");
        assertEquals("Surnames are not equal. Test S13.", "testsurname", st2.getSurname());
        assertEquals("Surnames are not equal. Test S14.", st2.getSurname(), student.getSurname());
    }
}