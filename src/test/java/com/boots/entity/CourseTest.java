package com.boots.entity;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class CourseTest {


    @PersistenceContext
    EntityManager em;

    @Test
    public void getId() {
        Course course = new Course();
        assertEquals("Returned param id is wrong. Test C1.",null, course.getId());
        course.setId(89L);
        assertEquals("Failed to set id or id is wrong. Test C2.", java.util.Optional.of(89L), java.util.Optional.of(course.getId()));

        Long u_id = Integer.toUnsignedLong(3);
        List<Course> m_course = em.createQuery("Select c from Course c where c.id = :u_id")
                .setParameter("u_id", u_id).getResultList();
        assertEquals(java.util.Optional.of(Integer.toUnsignedLong(3)), java.util.Optional.of(m_course.get(0).getId()));
    }

    @Test
    public void setId() {
        Course course = new Course();
        course.setId(76L);
        assertEquals("Failed to set id or id is wrong. Test C3.", java.util.Optional.of(76L), java.util.Optional.of(course.getId()));
    }

    @Test
    public void getName() {
        Course course = new Course();
        assertEquals("Names are not equal. Test C4.", null, course.getName());
        course.setName("testname");
        assertEquals("Names are not equal. Test C5.", "testname", course.getName());

        Long u_id = Integer.toUnsignedLong(3);
        List<Course> m_course = em.createQuery("Select c from Course c where c.id = :u_id")
                .setParameter("u_id", u_id).getResultList();
        assertEquals("Основы тестирования", m_course.get(0).getName());
    }

    @Test
    public void setName() {
        Course course = new Course();
        course.setName("testname");
        assertEquals("Names are not equal. Test C6.", "testname", course.getName());
        Course course2 = new Course();
        course2.setName("testname");
        assertEquals("Names are not equal. Test C7.", "testname", course2.getName());
        assertEquals("Names are not equal. Test C8.", course2.getName(), course.getName());
    }

    @Test
    public void getDescription() {
        Course course = new Course();
        assertEquals("Surnames are not equal. Test C9.", null, course.getDescription());
        course.setDescription("testdescription");
        assertEquals("Surnames are not equal. Test C10.", "testdescription", course.getDescription());

        Long u_id = Integer.toUnsignedLong(3);
        List<Course> m_course = em.createQuery("Select c from Course c where c.id = :u_id")
                .setParameter("u_id", u_id).getResultList();
        assertEquals("Курс по тестированию + описание", m_course.get(0).getDescription());
    }

    @Test
    public void setDescription() {
        Course course = new Course();
        assertEquals("Surnames are not equal. Test C11.", null, course.getDescription());
        course.setDescription("testdescription");
        assertEquals("Surnames are not equal. Test C12.", "testdescription", course.getDescription());
        Course course2 = new Course();
        course2.setDescription("testdescription");
        assertEquals("Surnames are not equal. Test C13.", "testdescription", course2.getDescription());
        assertEquals("Surnames are not equal. Test C14.", course2.getDescription(), course.getDescription());
    }

}