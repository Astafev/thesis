package com.boots.entity;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class MarkTest {

    @PersistenceContext
    EntityManager em;

    @Test
    public void getId() {
        Mark mark = new Mark();
        assertEquals("Returned param id is wrong. Test M1.",null, mark.getId());
        mark.setId(89L);
        assertEquals("Failed to set id or id is wrong. Test M2.", java.util.Optional.of(89L), java.util.Optional.of(mark.getId()));

        Long u_id = Integer.toUnsignedLong(35);
        List<Mark> main_mark = em.createQuery("Select m from Mark m where m.id = :u_id")
                .setParameter("u_id", u_id).getResultList();
        assertEquals(java.util.Optional.of(Integer.toUnsignedLong(35)), java.util.Optional.of(main_mark.get(0).getId()));

    }

    @Test
    public void setId() {
        Mark mark = new Mark();
        mark.setId(76L);
        assertEquals("Failed to set id or id is wrong. Test M3.", java.util.Optional.of(76L), java.util.Optional.of(mark.getId()));
    }

    @Test
    public void getMark() {
        Mark mark = new Mark();
        assertEquals("Returned param mark is wrong. Test M4.",null, mark.getMark());
        mark.setMark(7L);
        assertEquals("Failed to set mark or mark is wrong. Test M5.", java.util.Optional.of(7L), java.util.Optional.of(mark.getMark()));

        Long u_id = Integer.toUnsignedLong(35);
        List<Mark> main_mark = em.createQuery("Select m from Mark m where m.id = :u_id")
                .setParameter("u_id", u_id).getResultList();
        assertEquals(java.util.Optional.of(Integer.toUnsignedLong(6)), java.util.Optional.of(main_mark.get(0).getMark()));
    }

    @Test
    public void setMark() {
        Mark mark = new Mark();
        mark.setMark(8L);
        assertEquals("Failed to set mark or mark is wrong. Test M6.", java.util.Optional.of(8L), java.util.Optional.of(mark.getMark()));

    }

    @Test
    public void getStud_id() {
        Mark mark = new Mark();
        assertEquals("Returned param id is wrong. Test M7.",null, mark.getId());
        mark.setStud_id(89L);
        assertEquals("Failed to set id or id is wrong. Test M8.", java.util.Optional.of(89L), java.util.Optional.of(mark.getStud_id()));

        Long u_id = Integer.toUnsignedLong(35);
        List<Mark> main_mark = em.createQuery("Select m from Mark m where m.id = :u_id")
                .setParameter("u_id", u_id).getResultList();
        assertEquals(java.util.Optional.of(Integer.toUnsignedLong(4)), java.util.Optional.of(main_mark.get(0).getStud_id()));

    }

    @Test
    public void setStud_id() {
        Mark mark = new Mark();
        mark.setId(76L);
        assertEquals("Failed to set id or id is wrong. Test M9.", java.util.Optional.of(76L), java.util.Optional.of(mark.getId()));
    }

    @Test
    public void getCourse_id() {
        Mark mark = new Mark();
        assertEquals("Returned param id is wrong. Test M10.",null, mark.getCourse_id());
        mark.setCourse_id(89L);
        assertEquals("Failed to set id or id is wrong. Test M10.", java.util.Optional.of(89L), java.util.Optional.of(mark.getCourse_id()));

        Long u_id = Integer.toUnsignedLong(35);
        List<Mark> main_mark = em.createQuery("Select m from Mark m where m.id = :u_id")
                .setParameter("u_id", u_id).getResultList();
        assertEquals(java.util.Optional.of(Integer.toUnsignedLong(2)), java.util.Optional.of(main_mark.get(0).getCourse_id()));

    }

    @Test
    public void setCourse_id() {
        Mark mark = new Mark();
        mark.setCourse_id(89L);
        assertEquals("Failed to set id or id is wrong. Test M11.", java.util.Optional.of(89L), java.util.Optional.of(mark.getCourse_id()));
    }
}