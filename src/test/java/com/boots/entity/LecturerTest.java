package com.boots.entity;

import com.boots.service.LecturerService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.*;

public class LecturerTest {

    @Autowired
    private LecturerService lectServ;

    @Test
    public void getId() {
        Lecturer lecturer = new Lecturer();
        assertEquals("Returned param id is wrong. Test L1.",null, lecturer.getId());
        lecturer.setId(89L);
        assertEquals("Failed to set id or id is wrong. Test L2.", java.util.Optional.of(89L), java.util.Optional.of(lecturer.getId()));
    }

    @Test
    public void setId() {
        Lecturer lecturer = new Lecturer();
        lecturer.setId(76L);
        assertEquals("Failed to set id or id is wrong. Test L3.", java.util.Optional.of(76L), java.util.Optional.of(lecturer.getId()));
    }

    @Test
    public void getName() {
        Lecturer lecturer = new Lecturer();
        assertEquals("Names are not equal. Test L4.", null, lecturer.getName());
        lecturer.setName("testname");
        assertEquals("Names are not equal. Test L5.", "testname", lecturer.getName());
    }

    @Test
    public void setName() {
        Lecturer lecturer = new Lecturer();
        lecturer.setName("testname");
        assertEquals("Names are not equal. Test L6.", "testname", lecturer.getName());
        Lecturer lect2 = new Lecturer();
        lect2.setName("testname");
        assertEquals("Names are not equal. Test L7.", "testname", lect2.getName());
        assertEquals("Names are not equal. Test L8.", lect2.getName(), lect2.getName());
    }

    @Test
    public void getSurname() {
        Lecturer lecturer = new Lecturer();
        assertEquals("Names are not equal. Test L9.", null, lecturer.getSurname());
        lecturer.setSurname("testsurname");
        assertEquals("Names are not equal. Test L10.", "testsurname", lecturer.getSurname());
    }

    @Test
    public void setSurname() {
        Lecturer lecturer = new Lecturer();
        lecturer.setName("testname");
        assertEquals("Names are not equal. Test L11.", "testname", lecturer.getName());
        Lecturer lect2 = new Lecturer();
        lect2.setName("testname");
        assertEquals("Names are not equal. Test L12.", "testname", lect2.getName());
        assertEquals("Names are not equal. Test L13.", lect2.getName(), lect2.getName());
    }

    @Test
    public void getPatronymic() {
        Lecturer lecturer = new Lecturer();
        assertEquals("Names are not equal. Test L14.", null, lecturer.getPatronymic());
        lecturer.setPatronymic("testpatr");
        assertEquals("Names are not equal. Test L15.", "testpatr", lecturer.getPatronymic());
    }

    @Test
    public void setPatronymic() {
        Lecturer lecturer = new Lecturer();
        lecturer.setPatronymic("testpatr");
        assertEquals("Names are not equal. Test L16.", "testpatr", lecturer.getPatronymic());
        Lecturer lect2 = new Lecturer();
        lect2.setPatronymic("testpatr");
        assertEquals("Names are not equal. Test L17.", "testpatr", lect2.getPatronymic());
        assertEquals("Names are not equal. Test L18.", lect2.getPatronymic(), lect2.getPatronymic());
    }

    @Test
    public void getLecturerCourse(){
        List<Course> course = lectServ.findLecturerCourse(1L);
        assertEquals("Конструирование программного обеспечения", course.get(0).getName());
    }
}