<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
  <title>Главная</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">


  <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
  <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>
<div class="container">
  <h3>${pageContext.request.userPrincipal.name}</h3>
  <sec:authorize access="!isAuthenticated()">
    <h4><button type="button"><a href="/login">Войти</a></button></h4>
    <h4></h4>
    <h4><button type="button"><a href="/registration">Зарегистрироваться</a></button></h4>
    <h4></h4>
  </sec:authorize>
  <sec:authorize access="isAuthenticated()">
    <h4><button type="button"><a href="/logout">Выйти</a></button></h4>
    <h4></h4>
    <sec:authorize access="hasRole('ROLE_USER')">
      <h4><button type="button"><a href = "/studmarks">Оценки студента</a></button></h4>
      <h4></h4>
      <h4><button type="button"><a href="/courseslist">Список курсов студента</a></button></h4>
      <h4></h4>
    </sec:authorize>
    <sec:authorize access = "hasRole('ROLE_LECTURER')">
      <h4><button type="button"><a href="/coursemarks">Оценки за курс</a></button></h4>
      <h4></h4>
    </sec:authorize>
    <sec:authorize access = "hasRole('ROLE_ADMIN')">
      <h4><button type="button"><a href="/admin">Список пользователей</a></button></h4>
      <h4></h4>
    </sec:authorize>
  </sec:authorize>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>