package com.boots.entity.persisted;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "t_lecturer", schema = "public", catalog = "spring")
public class TLecturerEntity {
    private int id;
    private String name;
    private String surname;
    private String patronymic;
    //private int c_id;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "surname")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Basic
    @Column(name = "patronymic")
    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

//    @Basic
//    @Column(name = "course_id")
//    public int getC_id(){return c_id;}
//
//    public void setC_id(int c_id){this.c_id = c_id;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TLecturerEntity that = (TLecturerEntity) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(patronymic, that.patronymic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, patronymic);
    }
}
