package com.boots.entity.persisted;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "t_marks", schema = "public", catalog = "spring")
public class TMarksEntity {
    private int id;
    private Integer mark;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "mark")
    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TMarksEntity that = (TMarksEntity) o;
        return id == that.id &&
                Objects.equals(mark, that.mark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, mark);
    }
}
