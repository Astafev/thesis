package com.boots.entity.persisted;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "t_user_roles", schema = "public", catalog = "spring")
@IdClass(TUserRolesEntityPK.class)
public class TUserRolesEntity {
    private long userId;
    private long rolesId;

    @Id
    @Column(name = "user_id")
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Id
    @Column(name = "roles_id")
    public long getRolesId() {
        return rolesId;
    }

    public void setRolesId(long rolesId) {
        this.rolesId = rolesId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TUserRolesEntity that = (TUserRolesEntity) o;
        return userId == that.userId &&
                rolesId == that.rolesId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, rolesId);
    }
}
