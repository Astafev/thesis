package com.boots.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "t_student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

//    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JoinTable(name = "t_marks",
//               joinColumns = @JoinColumn(name = "stud_id"))
//    private List<Mark> stud_marks;

    public Student() {
    }

    public Student(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

//    public List<Mark> getStud_marks() {
//        return stud_marks;
//    }
//
//    public void setStud_marks(List<Mark> stud_marks) {
//        this.stud_marks = stud_marks;
//    }
}
