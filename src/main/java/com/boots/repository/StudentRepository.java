package com.boots.repository;

import com.boots.entity.Mark;
import com.boots.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student,Long> {

    public Student findStudentByNameAndSurname(String stud_name, String stud_surname);

    public Student findStudentById(Long id);
}
