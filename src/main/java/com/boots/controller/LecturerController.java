package com.boots.controller;

import com.boots.entity.Course;
import com.boots.entity.Lecturer;
import com.boots.entity.Student;
import com.boots.entity.User;
import com.boots.service.LecturerService;
import com.boots.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

@Controller
public class LecturerController {

    @Autowired
    private UserService userService;

    @Autowired
    private LecturerService lectService;

    @GetMapping("/coursemarks")
    public String studentMarks(Model model) {

        String username = userService.getCurrentUsername();
        List<Lecturer> lecturers = userService.findLecturerUser(username);
        Long course_id = lecturers.get(0).getId();
        List<Course> course = lectService.findLecturerCourse(course_id);
        HashMap<String, Integer> marks = null;
        String c_name = course.get(0).getName();
        try {
            marks = lectService.getCourseMarks(course.get(0).getName());
        } catch (Exception ex) {
        }

        model.addAttribute("coursename", c_name);
        model.addAttribute("courseMarks", marks);
        return "coursemarks";
    }
}

